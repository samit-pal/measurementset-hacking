"""
For runnis this code:  python3 msfile_merging.py band_name empty_filename.py
"""

import os
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('band_name', default=None, type= str , help ="BAND NAME OF SKA DATA")
#parser.add_argument('--init_range', default=None, type= int , help ="RANGE OF DATA POINT")
#parser.add_argument('--final_range', default=None, type= int , help ="RANGE OF DATA POINT")

parser.add_argument('model_msfile', default=None, type= str , help ="MODEL MSFILE NAME")
args=parser.parse_args()


main_path = os.path.join("/scratch/samit/SKADC3a/",args.band_name+"/")
msfile_model = os.path.join("/scratch/samit/SKADC3a/",args.model_msfile)

#init_range = args.init_range
#final_range = args.final_range
###### ------------------------------------- MS MERGING -------------------

def get_data(msfile):
    tb.open(msfile)
    data=tb.getcol('DATA')
    tb.close()
    tb.done()
    return data

def get_others(msfile):
    tb.open(msfile)
    uvw=tb.getcol('UVW')
    weight = tb.getcol('WEIGHT')
    sigma = tb.getcol('SIGMA')
    time = tb.getcol('TIME')
    tb.close()
    tb.done()
    return uvw, weight, sigma, time

data=[]
for k in range(901):
    msfile =  os.path.join(main_path,"ZW3_IFRQ_"+f"{k:04d}"+"_2min.ms")
    data.append(get_data(msfile))

data=np.asarray(np.squeeze(data))
data = data.reshape([1,data.shape[0],-1])    
uvw, weight, sigma,time  = get_others(msfile)
tb.open(msfile_model,nomodify=False)
tb.putcol("DATA",data)
tb.putcol("UVW",uvw)
tb.putcol("WEIGHT",weight)
tb.putcol("SIGMA",sigma)
tb.putcol("TIME",time)
tb.close()
tb.done()
print("\n\n --------------------------  MS FILE CONCATENATE  DONE ---------------------------------------\n\n")
print('------------------------------  END -----------------------------------------')


##############################################################################################################################################################

"""
run this code python3 msfile_merging.py band_name empty_filename.py
"""